import React, { Component } from 'react';

class HelloMessage extends Component {
    render() {
        return (
            <div className='hello-msg'>
                Hello {this.props.name}
            </div>
        );
    }
}

export default HelloMessage;