import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import HelloMessage from './wed3/HelloMessage';
import Counter from './wed3/Counter';
import SignupForm from './wed3/SignupForm';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>

        <h2>UI Components</h2>
        <HelloMessage name="React User"/>

        <h2>States</h2>
        <Counter />

        <h2>Forms</h2>
        <SignupForm />

      </div>
    );
  }
}

export default App;
